import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import PostList from "../../components/PostList/PostList";
import {fetchPosts} from "../../store/action/posts";

class Post extends Component {
    componentDidMount() {
        this.props.onFetchPosts();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Posts
                </PageHeader>

                {this.props.posts.map(post => (
                    <PostList
                        key={post._id}
                        id={post._id}
                        title={post.title}
                        description={post.description}
                        image={post.image}
                        user={post.user.username}
                        datetime={post.datetime}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        posts: state.posts.posts,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPosts: () => dispatch(fetchPosts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Post);