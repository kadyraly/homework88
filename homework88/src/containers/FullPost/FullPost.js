import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Image, PageHeader, Panel} from "react-bootstrap";

import {fetchPost} from "../../store/action/posts";
import ComForm from "../../components/ComForm/ComForm";
import {createCommit, fetchCommits} from "../../store/action/commits";
import Commit from "../Commit/Commit";

class FullPost extends Component {
    

    componentDidMount() {
        this.props.onFetchPost(this.props.match.params.id);
    }
    createCommit = (commitData, token) => {
        this.props.onCommitCreated(commitData, token);
    };


    render() {
        if(!this.props.post) {
            return <div>Loading</div>
        }
        return (
            <Fragment>
                <PageHeader>
                    {this.props.post.title}
                </PageHeader>
                <Panel>
                    <Panel.Body>
                        <Image
                            style={{width: '100px', marginRight: '10px'}}
                            src={this.props.post.image}
                            thumbnail
                        />
                        {this.props.post.description}
                    </Panel.Body>
                    <Panel.Footer>by {this.props.post.user.username} {this.props.post.datetime}</Panel.Footer>
                </Panel>
                <ComForm postId={this.props.match.params.id} onSubmit={this.createCommit}/>
                <Commit postId={this.props.match.params.id} />
            </Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        post: state.posts.post,
        user: state.users.user,
        commits: state.commits.commits
    }
};


const mapDispatchToProps = dispatch => {
    return {
        onFetchPost: (id) => dispatch(fetchPost(id)),
        onFetchCommits: (id) => dispatch(fetchCommits(id)),
        onCommitCreated: (commitData, token) => {
            return dispatch(createCommit(commitData, token))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FullPost);