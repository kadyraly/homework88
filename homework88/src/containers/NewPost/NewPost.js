import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";


import PostForm from "../../components/PostForm/PostForm";
import {createPost} from "../../store/action/posts";

class NewPost extends Component {
    createPost = (postData, token) => {
        this.props.onPostCreated(postData, token).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New post</PageHeader>
                <PostForm onSubmit={this.createPost} />
            </Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        user: state.users.user
    }

};

const mapDispatchToProps = dispatch => {
    return {
        onPostCreated: (postData, token) => {
            return dispatch(createPost(postData, token))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);