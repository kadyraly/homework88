import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ComList from "../../components/ComList/ComList";
import {fetchCommits} from "../../store/action/commits";

class Commit extends Component {
    componentDidMount() {
        this.props.onFetchCommits(this.props.postId);
    }



    render() {
        console.log(this.props)
        return (
            <Fragment>
                <PageHeader>
                    Comments
                </PageHeader>

                {this.props.commits.map(commit => (
                    <ComList
                        key={commit._id}
                        id={commit._id}
                        comment={commit.comment}
                        user={commit.user.username}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        commits: state.commits.commits,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchCommits: (id) => dispatch(fetchCommits(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Commit);