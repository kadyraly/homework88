import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import NewPost from "./containers/NewPost/NewPost";
import Post from "./containers/Post/Post";

import FullPost from "./containers/FullPost/FullPost";


class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Post} />
                    <Route path="/posts/new" exact component={NewPost} />
                    <Route path="/posts/:id" exact component={FullPost} />
                    <Route path="/register" exact component={Register} />
                    <Route path="/login" exact component={Login} />
                </Switch>
            </Layout>
        )
    }
}

export default App;