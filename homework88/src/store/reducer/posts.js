
import {FETCH_POST_SUCCESS, FETCH_POSTS_SUCCESS} from "../action/actionTypes";

const initialState = {
    posts: [],
    post: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_POSTS_SUCCESS:
            return {...state, posts: action.posts};
        case FETCH_POST_SUCCESS:
            console.log(action.post);
            return {...state, post: action.post};
        default:
            return state;
    }
};

export default reducer;