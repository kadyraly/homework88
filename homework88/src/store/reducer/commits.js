import {FETCH_COMMITS_SUCCESS} from "../action/actionTypes";

const initialState = {
    commits: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_COMMITS_SUCCESS:

            return {...state, commits: action.commits};
        default:
            return state;
    }
};

export default reducer;