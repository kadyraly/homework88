import {applyMiddleware, combineReducers, compose, createStore} from "redux";

import thunkMiddleware from "redux-thunk";
import {routerMiddleware, routerReducer} from "react-router-redux";

import createHistory from "history/createBrowserHistory";

import postsReducer from "./reducer/posts";
import commitsReducer from './reducer/commits';
import usersReducer from "./reducer/users";
import  {saveState, loadState} from "./localStorage";

const rootReducer = combineReducers({
    posts: postsReducer,
    users: usersReducer,
    commits: commitsReducer,
    routing: routerReducer
});

export const history = createHistory();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));


const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({
        users: store.getState().users
    });
});

export default store;
