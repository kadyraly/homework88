import axios from '../../axios-api';
import {CREATE_COMMIT_SUCCESS, FETCH_COMMITS_SUCCESS} from "./actionTypes";


export const fetchCommitsSuccess = commits => {
    return {type: FETCH_COMMITS_SUCCESS, commits};
};

export const fetchCommits = (id) => {
    return dispatch => {
        axios.get('/commits?post=' + id).then(
            response => {
                dispatch(fetchCommitsSuccess(response.data))
            }
        );
    }
};

export const createCommit = (commitData)=> {
    return (dispatch, getState) => {
        return axios.post('/commits', commitData, {headers: {'Token': getState().users.user.token}}).then(
            response => {
                dispatch(fetchCommits(response.data.post))
            }
        );
    };
};