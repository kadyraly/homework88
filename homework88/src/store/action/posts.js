import axios from '../../axios-api';
import {CREATE_POST_SUCCESS, FETCH_POST_SUCCESS, FETCH_POSTS_SUCCESS} from "./actionTypes";


export const fetchPostsSuccess = posts => {
    return {type: FETCH_POSTS_SUCCESS, posts};
};

export const fetchPostSuccess = post => {
    return {type: FETCH_POST_SUCCESS, post};
};

export const fetchPosts = () => {
    return dispatch => {
        axios.get('/posts').then(
            response => dispatch(fetchPostsSuccess(response.data))
        );
    }
};

export const fetchPost = (id) => {
    return dispatch => {
        axios.get('/posts/' + id).then(
            response => {
                console.log(response.data)
                dispatch(fetchPostSuccess(response.data))
            }
        );
    }
};

export const createPostSuccess = (posts) => {
    return {type: CREATE_POST_SUCCESS, posts};
};

export const createPost = (postData)=> {
    return (dispatch, getState) => {
        return axios.post('/posts', postData, {headers: {'Token': getState().users.user.token}}).then(
            response => dispatch(createPostSuccess())
        );
    };
};