import React from 'react';
import {Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

const ComList = props => {

    return (
        <Panel>
            <Panel.Body>
                {props.comment}
            </Panel.Body>

        </Panel>
    );
};

ComList.propTypes = {
    id: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired
};

export default ComList;