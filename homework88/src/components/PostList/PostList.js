import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/image/not-found.png';

const PostList = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }

    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '100px', marginRight: '10px'}}
                    src={image}
                    thumbnail
                />
                {props.datetime} by {props.user}
                <Panel.Body>
                    <Link to={'/posts/' + props.id}>
                         {props.title}
                    </Link>
                </Panel.Body>
            </Panel.Body>
        </Panel>
    );
};

PostList.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    datetime: PropTypes.string,
    user: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string
};

export default PostList;