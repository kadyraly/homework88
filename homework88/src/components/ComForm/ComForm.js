import React, {Component, Fragment} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

class ComForm extends Component {
    state = {
        comment: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit({comment: this.state.comment, post: this.props.postId});
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {

        return (
            <Fragment>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormGroup controlId="comment">
                        <Col componentClass={ControlLabel} sm={2}>
                            Comment
                        </Col>
                        <Col sm={10}>
                            <FormControl
                                type="text" required
                                placeholder="Enter comment"
                                name="comment"
                                value={this.state.comment}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button bsStyle="primary" type="submit">Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}


export default ComForm;