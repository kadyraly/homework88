const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: {
        type: String, required: true
    },
    description: String,
    image: String,
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    datetime: {
        type: String
    }
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;