const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommitSchema = new Schema({
    comment: {
        type: String, required: true
    },
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
        required: true
    },
    user: {
        type: Schema.Types.ObjectId, ref: 'User', required: true
    }
});

const Commit = mongoose.model('Commit', CommitSchema);

module.exports = Commit;