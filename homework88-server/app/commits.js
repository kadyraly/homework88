const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Commit = require('../model/Commit');
const User = require('../model/User');




const router = express.Router();

const createRouter = (db) => {

    router.get('/',(req, res) => {
        if(req.query.post) {
            Commit.find({post: req.query.post}).populate('user post')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }

    });

    router.post('/',  async (req, res) => {
        let token = req.get('Token');

        if(!token) return res.sendStatus(401);

        let user = await User.findOne({token: token});

        if(!user) return res.sendStatus(401);

        const comData = req.body;

        console.log(user)
        comData.user = user._id;

        console.log(comData)

        const com = new Commit(comData);

        com.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });



    return router;
};

module.exports = createRouter;