const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Post = require('../model/Post');
const User = require('../model/User');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {

    router.get('/', async (req, res) => {
        Post.find().populate('user')
            .then(results => res.send(results.reverse()))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', upload.single('image'), async (req, res) => {
        const postData = req.body;
        if (req.file) {
            postData.image = req.file.filename;
        } else {
            postData.image = null;
        }
        const tokenId = req.get('Token');
        const user = await User.findOne({token: tokenId}).select('_id');
        if (user) {
                postData.user = user._id;
                postData.datetime = new Date();
                const post = new Post(postData);
                await post.save();
                res.send({post});

        } else {
            res.sendStatus(401)
        }
    });


    router.get('/:id', (req, res) => {
        const id = req.params.id;
        Post.findOne({_id: id}).populate('user')
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;